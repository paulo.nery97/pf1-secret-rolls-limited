const CFG = {
	module: 'mkah-pf1-secret-rolls',
	SETTINGS: {
		superSecret: 'no-one-knows',
		secretDCs: 'hide-DCs',
	},
	requiredPermissionLevel: CONST.ENTITY_PERMISSIONS.LIMITED,
};

// Eslint
/* global RollPF */

/**
 * Get basic user data. Needed for early retrieval of user data.
 */
function getUserFromData() {
	const data = game.data,
		id = data?.userId;
	const userData = id ? data.users?.find(u => u._id === id) : null;
	const user = {
		id,
		role: userData?.role ?? 0,
		name: userData?.name,
		get isGM() {
			return this.role >= CONST.USER_ROLES.ASSISTANT;
		}
	};
	return user;
}

/**
 * Tests if at least limited permission is present.
 */
function isLimited(doc, user) {
	user = user ?? game.user ?? getUserFromData();
	return doc.data.permission[user.id] >= CFG.requiredPermissionnLevel || doc.data.permission.default >= CFG.requiredPermissionnLevel;
}

/**
 * Exhaustively test if user has sufficent permissions to chat message contents.
 */
function hasSufficientPermission(cm) {
	if (cm.isAuthor || cm.isOwner)
		return true; // simple trust, the user created the message

	if (cm.itemSource) return isLimited(cm.itemSource);

	const speaker = cm.data.speaker;
	if (!speaker) return false; // no speaker, assume irrelevant or insufficient (this honestly shouldn't happen)

	const actor = ChatMessage.getSpeakerActor(speaker);
	// const actor = game.actors.get(speaker.actor);
	if (actor) return isLimited(actor);

	const scene = actor ? null : game.scenes.get(speaker.scene);
	const token = scene ? scene.tokens.get(speaker.token) : null;
	if (token) return isLimited(token);

	return false;
}

/**
 * Wipe away all those nasty roll details.
 */
function scrubRollDetails(html, secret = false) {
	// Remove all but the die roll from breakdowns
	html.querySelectorAll('.dice-tooltip')
		?.forEach(function () {
			if (secret) {
				this.remove();
			}
			else
				Array.from(this.querySelectorAll('.tooltip-part')).splice(1).forEach(d => d.remove());
		});
}

/**
 * @param {ChatMessage} cm
 * @param {JQuery} jq
 * @param {Object} opts
 */
function scrubChatCard(cm, jq, opts) {
	if (hasSufficientPermission(cm))
		return;

	const secretify = game.settings.get(CFG.module, CFG.SETTINGS.superSecret);

	const html = jq[0];

	// Listens to inline rolls being opened so their contents can be filtered.
	const ir = html.querySelectorAll('.inline-roll[data-roll]');
	for (const ird of ir) {
		const sroll = RollPF.fromData(JSON.parse(unescape(ird.dataset.roll)));
		ird.classList.add('scrubbed');
		if (!secretify) {
			// Remake the roll data to not need an event listener for the opened roll data
			const diceTotal = sroll.dice.reduce((total, d) => d.total, 0);
			const totalMod = sroll.total - diceTotal;

			const nroll = sroll.clone();

			const newTerms = nroll.dice.reduce((terms, d, index, array) => {
				d.options.flavor = undefined;
				d._evaluated = true;
				d.results.push(...sroll.dice[index].results);
				d._total = d.results.reduce((total, r) => r.active ? r.result : 0, 0);
				console.log(d);
				terms.push(d);
				if (index < array.length - 1) terms.push(new OperatorTerm({ operator: '+' }));
				return terms;
			}, []);

			if (totalMod != 0) {
				if (nroll.dice.length)
					newTerms.push(new OperatorTerm({ operator: '+' }));
				newTerms.push(new NumericTerm({ number: totalMod }));
			}

			nroll.terms = newTerms;
			nroll._total = sroll._total;
			nroll._evaluated = true;

			nroll._formula = RollPF.getFormula(nroll.terms);

			console.log(nroll);

			ird.dataset.roll = escape(JSON.stringify(nroll));
			ird.title = nroll.formula;

			console.log(sroll.formula);
			console.log(nroll.formula, nroll);
			console.log(nroll._formula);
		}
		else {
			// Replace inline roll data with fake 0 to avoid unnecessary errors.
			ird.dataset.roll = escape(JSON.stringify(RollPF.safeRoll(`${sroll.total}`)));
			ird.title = `${sroll.total}`;
			ird.classList.remove('inline-result');
		}
	}

	// Non-inline rolls can be scrubbed as is.
	scrubRollDetails(html, secretify);

	// Obscure DC
	if (game.settings.get(CFG.module, CFG.SETTINGS.secretDCs) && cm.itemSource?.hasSave) {
		html.querySelectorAll('button[data-action="save"]')
			?.forEach(function (el) {
				let sav = el.dataset.type;
				sav = sav[0].toUpperCase() + sav.slice(1);
				el.innerText = game.i18n.translations.PF1[`SavingThrow${sav}`];
				el.removeAttribute('data-dc');
			});
	}

	// Super scrub, might cause problems
	// cm.data.roll = null;
}

Hooks.on('renderChatMessage', scrubChatCard);

const debouncedReload = foundry.utils.debounce(() => window.location.reload(), 250);

Hooks.once('init', () => {
	game.settings.register(
		CFG.module,
		CFG.SETTINGS.superSecret,
		{
			name: 'Complete secrecy',
			hint: 'Hides all roll details if enabled, only showing the final result. If disabled die rolls are still distinguished. Toggling this will reload the app for all players.',
			config: true,
			scope: 'world',
			type: Boolean,
			default: false,
			onChange: () => game.user.isGM ? true : debouncedReload()
		}
	);

	game.settings.register(
		CFG.module,
		CFG.SETTINGS.secretDCs,
		{
			name: 'Hide DCs',
			hint: 'Hide DC from saving throw roll prompts. Toggling this will reload the app for all players.',
			config: true,
			scope: 'world',
			type: Boolean,
			default: false,
			onChange: () => game.user.isGM ? true : debouncedReload()
		}
	);
});

# This is mostly a copy of https://gitlab.com/mkahvi/fvtt-micro-modules/-/tree/master/pf1-secret-rolls 

# Pathfinder 1 Secret Rolls

Makes rolls made by those you don't have at minimum observer permission for less descript.

  ⛔ Information blackhole is not good for anything but horror campaigns. For others, it just seems toxic to me.

## Screencap

Left is GM/owner, right is player without observer or higher permission.

![Screencap](./screencap.png)

## Install

Manifest URL: https://gitlab.com/paulo.nery97/pf1-secret-rolls-limited/-/raw/main/module.json

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE), and by extension under [FVTT's Module Development License](https://foundryvtt.com/article/license/).
